<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

/**
 *
 * @author koby
 */
interface CacheInterface {
    //put your code here
    public function get($key);
    public function has($key);
    public function set($key,$value,$expiration=3000);
}
