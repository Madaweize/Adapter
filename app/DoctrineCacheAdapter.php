<?php

/*
 *Le principe del'adaptater est de modifier le comportement de l'objet de base
 * Cet adaptater fonctionne comme ci
 * il va prendre dans son constructeur l'objet original et il va ensuite mappé
 * les differentes methodes de notre interface au methodes original de l'objet
 */

namespace App;

/**
 * Description of DoctrineCacheAdapter
 *
 * @author koby
 */
class DoctrineCacheAdapter implements CacheInterface{
    //put your code here
    
    protected $cache;
    
    
    public function __construct(\Doctrine\Common\Cache\FilesystemCache $cache) {
        
        $this->cache = $cache;
    }
    
    
    public function get($key) {
        return $this->cache->fetch($key);
    }

    public function has($key) {
        return $this->cache->contains($key);
    }

    public function set($key, $value, $expiration = 3000) {
        return $this->cache->save($key, $value, $expiration);
    }

}
