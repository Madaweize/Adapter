<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

/**
 * Description of Hello
 *
 * @author koby
 */
class Hello {
    //put your code here
    
   
    public function sayhello(CacheInterface $cache){
       if($cache->has('hello')){
           return $cache->get('hello');
       } else {
            sleep(10);
           $content= 'Bonjour';
           $cache->set('hello', $content);
           return $content;
       }
    }
}
